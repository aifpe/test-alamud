from .effect import Effect3
from mud.events import TransformeEvent

class TransformationPropEffect(Effect3):
    EVENT = TransformeEvent
    
    fichier = open("/home/pallu/Documents/SEMESTRE 2/PERIODE 3/GIT/test-alamud/copie-de-alamud.git/mud/games/jeu-de-l-annee/salleinitial.yml","w")
    fichier.write("---\n")
    fichier.write("id: mystere-000\n")
    fichier.write("type: Location\n")
    fichier.write("contains:\n")
    fichier.write("  - clef-001\n")
    fichier.write("events:\n")
    fichier.write("  info:\n")
    fichier.write('    actor: "Salle mystere"\n')
    fichier.write("  look:\n")
    fichier.write("    actor: |\n")
    fichier.write("      Vous êtes sur le porche du département info.\n")
    fichier.write("      Au nord-est, une porte donne accès au hall du\n")
    fichier.write("      département. Au sud-ouest, une allée mène au\n")
    fichier.write("      parking de l'IUT.\n")
    fichier.write("\n")
    fichier.write("---\n")
    fichier.write("\n")
    fichier.write("id: portal-debut-mystere-000\n")
    fichier.write("type: Portal\n")
    fichier.write("exits:\n")
    fichier.write("  - id: mystere-000-ouest\n")
    fichier.write("    location: mystere-000\n")
    fichier.write("    direction: ouest\n")
    fichier.write("  - id: parking-000-est\n")
    fichier.write("    location: parking-000\n")
    fichier.write("    direction: est\n")
    fichier.write("---\n")
    fichier.write("id: clef-001\n")
    fichier.write("type: Thing\n")
    fichier.write("props:\n")
    fichier.write("  - takable\n")
    fichier.write("  - key-for-salle-05\n")
    fichier.write("names   :\n")
    fichier.write("  - clef\n")
    fichier.write("  - cle\n")
    fichier.write("  - clé\n")
    fichier.write("gender: fem\n")
    fichier.write("events:\n")
    fichier.write("  info:\n")
    fichier.write('    actor: "Une clef."\n')
    fichier.write("  look:\n")
    fichier.write("    actor: |\n")
    fichier.write("     Salle 05\n")
    fichier.close()




    

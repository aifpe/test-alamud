

from .event import Event3


class TransformeEvent(Event3):
	NAME = "transforms"
	
	def perform(self):
		if not self.object.has_prop("part") or not self.object2.has_prop("part"):
			self.fail()
			return self.inform("transforms.failed")
		self.inform("transforms")
